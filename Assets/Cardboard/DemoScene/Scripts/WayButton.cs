﻿//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections;
//using UnityEngine.SceneManagement;
//
//public class WayButton : GazedButtonClass {
//	public GameObject unityChan;
//
//	// GazedButtonClassのDecisionButton()をOnMenuDisplayButton()を呼び出す関数にオーバーライドする
//	public void DecisionButton(string decisionObjName) {
//		DecisionWayButton (decisionObjName);
//	}
//
//	public string DecideEnemyWay(int enemyWayNum, string playerWayName) {
//		GameObject[] jankenWayButtons;
//		jankenWayButtons = GameObject.FindGameObjectsWithTag ("GameButton");
//		gameManager.progressText.text = "ポン!";
//		foreach (GameObject wayObject in jankenWayButtons) {
//			if (wayObject.name == playerWayName) {
//				wayObject.SetActive (true);
//				wayObject.GetComponent<Button> ().interactable = false;
//			} else {
//				wayObject.SetActive (false);
//			}
//		}
//		return jankenWayButtons [enemyWayNum].name;
//	}
//
//	public void DecisionWayButton(string decisionWayName) {
//		string enemyWayTypeName;
//		int randomNum = (int)Random.Range (0f, 3f);
//		enemyWayTypeName = DecideEnemyWay (randomNum, decisionWayName);
//		// ジャンケンの結果に応じたシーンに遷移するコルーチンを動かす
//		StartCoroutine (JankenResult (JudgeRegardingPlayerWay (enemyWayTypeName, decisionWayName)));
//	}
//
//	// プレイヤーが勝ったかどうかを判定する関数
//	public virtual int JudgeRegardingPlayerWay(string enemyWay, string playerWay) {
//		switch (enemyWay) {
//		case "GooButton":
//			unityChan.GetComponent<Animator> ().SetTrigger ("GooTrigger");
//			return 1;	// ジャンケンシーンの番号を返す
//			break;
//		case "ChokiButton":
//			unityChan.GetComponent<Animator> ().SetTrigger ("ChokiTrigger");
//			return 2;	// PlayerTurnのシーンの番号を返す
//			break;
//		default:
//			unityChan.GetComponent<Animator> ().SetTrigger ("PaaTrigger");
//			return 3;	// EnemyTurnのシーンの番号を返す
//			break;
//		}
//	}
//
//	// ジャンケンの結果を受け取り、シーン遷移するコルーチン
//	private IEnumerator JankenResult(int resultNum) {
//		if (resultNum == 1) {
//			PublicStaticInitializeManager.aikoFlag = true;
//		} else {
//			PublicStaticInitializeManager.aikoFlag = false;
//		}
//		yield return new WaitForSeconds (2.0f);
//		SceneManager.LoadScene (resultNum);	// シーンリセット
//	}
//}
