﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleManager : MonoBehaviour {
	public static bool VRModeSwitch = true;	// VR視点切り替えボタンの選択が決定されたかどうかのフラグ
	public static bool gameEndFlag = false;	// あっち向いてホイの結果を判定するフラグ
	public static bool aikoFlag = false;	// ジャンケンの結果あいこになったかどうかを判定するフラグ

	public Cardboard VRMode;
	public Text modeChangingVRText;

	void Start() {
		VRModeKeeping ();
	}

	void VRModeKeeping() {
		// VR視点切り替えボタンを押して
		// VRModeSwitch = true : 2眼
		// VRModeSwitch = false : 単眼
		if (VRModeSwitch) {
			VRMode.VRModeEnabled = true;
			modeChangingVRText.text = "VR:OFF";
		} else {
			VRMode.VRModeEnabled = false;
			modeChangingVRText.text = "VR:ON";
		}
	}
}
