﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

public class GazingButton : MonoBehaviour {
	public float progressLimitTime;		// Targettingが完了するまでの時間でインスペクター上で変更できる
	public Image progressCircleImage;
	public GazedButtonClass gazedButtonScript;

	// Awake()のタイミングでprogressCircleImageを表示してfillAmountを0に初期化
	void Awake() {
		progressCircleImage.enabled = true;
		progressCircleImage.fillAmount = 0;
	}

	// ボタンにレティクルを当て続けると時間経過でfillAmountを増やしていくことで画像を表示させる
	// そのまま一定時間が経過するとみていたボタンに応じた処理がかかる
	public void DisplayProgressCircleImage(bool isTargetting) {
		if (isTargetting) {	// ボタンにレティクルを当てている状態
			// 残り時間を表す画像が非表示である場合、表示させる
			if (!progressCircleImage.enabled) {
				progressCircleImage.enabled = true;
			}
			StartCoroutine ("ProgressGazingButtonCoroutine");
		} else {	// レティクルがボタンから離れている状態
			progressCircleImage.fillAmount = 0;	// Targetting画像の表示割合がリセットされる
			progressCircleImage.enabled = false;
			StopCoroutine ("ProgressGazingButtonCoroutine");
		}
	}

	public IEnumerator ProgressGazingButtonCoroutine() {
		float progressTime = 0;
		while (progressTime <= progressLimitTime) {
			// レティクルをボタンに当てている間の時間を経過させる
			progressTime += Time.deltaTime;
			// 時間経過に伴い円形に表示する画像の表示割合
			progressCircleImage.fillAmount = progressTime / progressLimitTime;
			// whileループが一瞬で回らないようにする
			yield return new WaitForSeconds (0);
		}
		gazedButtonScript.DecideButton ();
		progressCircleImage.enabled = false;
	}
}

