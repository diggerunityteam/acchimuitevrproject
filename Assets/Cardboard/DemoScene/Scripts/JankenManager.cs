﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class JankenManager : MonoBehaviour {
	// このクラス内でも使うし、ほかのクラスから参照して使うこともあるメンバ
	public Canvas selectionGameButtonCanvas;
	public Canvas menuPanelCanvas;
	public Canvas displayMenuPanelCanvas;
	public Text progressText;
	public Cardboard VRMode;
	public Text modeChangingVRText;

	// 他のクラスから参照して使うメンバ
	public GameObject unityChan;

	// Use this for initialization
	void Start () {
		menuPanelCanvas.enabled = false;
		displayMenuPanelCanvas.enabled = true;
		progressText.text = "";
		StartCoroutine(GameStartProgress());
		// VRの切り替え状態を保持する
		VRModeKeeping ();
	}

	void VRModeKeeping() {
		// VR視点切り替えボタンを押して
		// VRModeSwitch = true : 2眼
		// VRModeSwitch = false : 単眼
		if (TitleManager.VRModeSwitch) {
			VRMode.VRModeEnabled = true;
			modeChangingVRText.text = "VR:OFF";
		} else {
			VRMode.VRModeEnabled = false;
			modeChangingVRText.text = "VR:ON";
		}
	}

	// シーン開始からグー・チョキ・パーボタンが表示されるまでの流れをまとめたコルーチン
	public virtual IEnumerator GameStartProgress() {
		selectionGameButtonCanvas.enabled = false;
		if (TitleManager.aikoFlag) {
			progressText.text = "あい…";
			yield return new WaitForSeconds (1.0f);
			progressText.text = "こで…";
		} else {
			progressText.text = "ジャン…";
			yield return new WaitForSeconds (1.0f);
			progressText.text = "ケン…";
		}
		yield return new WaitForSeconds (2.0f);
		// この段階でメニューパネルが表示されていたら、ジャンケンボタンを表示しない
		if (!menuPanelCanvas.enabled) {
			selectionGameButtonCanvas.enabled = true;
		}
	}
}