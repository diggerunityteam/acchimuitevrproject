﻿using UnityEngine;
using System.Collections;
using System;

public class FaceAnim : MonoBehaviour {
	// 1:最後までアニメーションをかける
	// 0:アニメーションをかけない
	// 0～1はアニメーションどこまでかけるかの割合になる
	[SerializeField, Range(0, 1)]
	private float current;
	private Animator anim = null;	// このスクリプトを使っているオブジェクトのAnimatorコンポーネント
	// 表情のアニメーションクリップを列挙型変数processingTypeの数だけ格納数を持つ配列
	public AnimationClip[] animState;

	// 引数に応じた表情アニメーションを動かす
	public void ChangeFaceAnimation(int processingNum) {
		if (anim == null) {	// animがヌルならば
			// このスクリプトを使っているオブジェクトのAnimatorコンポーネントを取得
			anim = GetComponent<Animator> ();
		}
		// 引数で指定された配列番号の要素に格納されているアニメーションデータを設定する
		anim.CrossFade (animState[processingNum].name, 0);
		// 設定した表情アニメーションを使う
		anim.SetLayerWeight (1, current);
	}
}
