﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class AcchiManager : JankenManager {

	// ジャンケンボタンを表示させるコルーチンを方向ボタンを表示させるコルーチンにオーバーライドさせた
	public override IEnumerator GameStartProgress() {
		selectionGameButtonCanvas.enabled = false;
		FaceAnim (0);	// FaceAnimコンポーネントの有無を確認してから表情アニメーションを再生
		progressText.text = "あっち…";
		yield return new WaitForSeconds (1.0f);
		FaceAnim (1);	// FaceAnimコンポーネントの有無を確認してから表情アニメーションを再生
		progressText.text = "むいて…";
		yield return new WaitForSeconds (1.0f);
		FaceAnim (2);	// FaceAnimコンポーネントの有無を確認してから表情アニメーションを再生
		// この時点でメニューパネルが表示されていたら方向ボタンを表示させない
		if (!menuPanelCanvas.enabled) {
			selectionGameButtonCanvas.enabled = true;
		}
	}

	// FaceAnimコンポーネントがnullでなければ表情アニメーションを開始する
	public void FaceAnim(int faceAnimNum) {
		if (unityChan.GetComponent<FaceAnim> () != null) {
			unityChan.GetComponent<FaceAnim>().ChangeFaceAnimation (faceAnimNum);
		}
	}
}