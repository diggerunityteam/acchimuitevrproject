﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GazedButtonClass : MonoBehaviour {
	public GameObject gameManager;
	public int sceneNum;
	Animator unityChanJankenAnim;

	// どのボタンを決定したかをタグ名で判定し、それぞれのボタンの処理を実行する
	public void DecideButton() {
		switch (gameObject.tag) {
		case "VRModeButton":
			VRModeMenu ();
			break;
		case "DisplayMenuButton":
			OnMenuDisplayButton ();
			break;
		case "GameButton":
			gameManager.GetComponent<JankenManager>().displayMenuPanelCanvas.enabled = false;
			DecideGameButton (gameObject.name);
			break;
		default:
			SceneTransition ();
			break;
		}
	}

	public void VRModeMenu() {
		if (TitleManager.VRModeSwitch) {
			// 2眼のときに単眼に切り替えるフラグを立てる
			TitleManager.VRModeSwitch = false;
		} else {
			// 単眼のときに2眼に切り替えるフラグを立てる
			TitleManager.VRModeSwitch = true;
		}
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);	// シーンリセット
	}
		
	// sceneNumに応じたシーンに遷移する
	public void SceneTransition() {
		SceneManager.LoadScene (sceneNum);
	}

	// メニューパネルを表示し、ほかのCanvasを非表示にする
	public void OnMenuDisplayButton() {
		JankenManager canvasEnabled = gameManager.GetComponent<JankenManager> ();
		canvasEnabled.menuPanelCanvas.enabled = true;
		canvasEnabled.progressText.enabled = false;
		canvasEnabled.displayMenuPanelCanvas.enabled = false;
		if (canvasEnabled.selectionGameButtonCanvas != null) {
			canvasEnabled.selectionGameButtonCanvas.enabled = false;
		}
	}

	public virtual void DecideGameButton(string decisionWayName) {
		if (TitleManager.aikoFlag) {
			gameManager.GetComponent<JankenManager> ().progressText.text = "しょ!";
		} else {
			gameManager.GetComponent<JankenManager>().progressText.text = "ポン!";
		}
		int randomNum = (int)Random.Range (0f, 3f);
		// ジャンケンの結果に応じたシーンに遷移するコルーチンを動かす
		StartCoroutine (JankenResult (JudgeRegardingPlayerWay (DecideWay (randomNum, decisionWayName), decisionWayName)));
	}

	// 決定したボタン以外を非表示にし、対戦相手の手を決定する
	public string DecideWay(int enemyWayNum, string playerWayName) {
		GameObject[] gameWayButtons;
		gameWayButtons = GameObject.FindGameObjectsWithTag ("GameButton");
		foreach (GameObject wayObject in gameWayButtons) {
			if (wayObject.name == playerWayName) {	// 決定したボタンを表示しつつボタンの機能を無効にする
				wayObject.SetActive (true);
				wayObject.GetComponent<Button> ().interactable = false;
			} else {	// 選択しなかったボタンを非表示にする
				wayObject.SetActive (false);
			}
		}
		return gameWayButtons [enemyWayNum].name;
	}

	// ジャンケンの結果に応じたシーンに遷移する番号を返す
	public int JudgeRegardingPlayerWay(string enemyWay, string playerWay) {
		unityChanJankenAnim = gameManager.GetComponent<JankenManager>().unityChan.GetComponent<Animator>();

		if (playerWay == enemyWay) {
			unityChanJankenAnim.SetTrigger (enemyWay + "Trigger");
			return 1;	// ジャンケンシーンの番号を返す
		} else {
			switch (enemyWay) {
			case "GooButton":
				unityChanJankenAnim.SetTrigger ("GooButtonTrigger");
				break;
			case "ChokiButton":
				unityChanJankenAnim.SetTrigger ("ChokiButtonTrigger");
				break;
			default:
				unityChanJankenAnim.SetTrigger ("PaaButtonTrigger");
				break;
			}
			return JudgePlayerWay (enemyWay, playerWay);
		}
	}

	// ジャンケンの結果があいこでなかったときの判定法
	int JudgePlayerWay(string judgementEnemyWay, string judgementPlayerWay) {
		if (((judgementEnemyWay == "GooButton") && (judgementPlayerWay == "ChokiButton")) ||
			((judgementEnemyWay == "ChokiButton") && (judgementPlayerWay == "PaaButton")) ||
			((judgementEnemyWay == "PaaButton") && (judgementPlayerWay == "GooButton"))) {
			return 3;	// EnemyTurnのシーン番号を返す
		} else {
			return 2;	// PlayerTurnのシーン番号を返す
		}
	}

	// ジャンケンの結果を受け取り、シーン遷移するコルーチン
	public IEnumerator JankenResult(int resultNum) {
		if (resultNum == 1) {
			TitleManager.aikoFlag = true;
		} else {
			TitleManager.aikoFlag = false;
		}
		yield return new WaitForSeconds (1.0f);
		SceneManager.LoadScene (resultNum);	// ジャンケンの結果に応じたシーンに遷移
	}

}