﻿//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections;
//using UnityEngine.SceneManagement;
//
//public class VRModeButton : MonoBehaviour {
//	public PublicStaticInitializeManager gameManager;
//	public int sceneNum;
//
//	// Use this for initialization
//	void Start () {
//		VRModeKeeping ();
//	}
//
//	public void VRModeMenu() {
//		if (PublicStaticInitializeManager.VRModeSwitch) {
//			// 2眼のときに単眼に切り替えるフラグを立てる
//			PublicStaticInitializeManager.VRModeSwitch = false;
//		} else {
//			// 単眼のときに2眼に切り替えるフラグを立てる
//			PublicStaticInitializeManager.VRModeSwitch = true;
//		}
//		VRModeKeeping ();
//		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);	// シーンリセット
//	}
//
//	void VRModeKeeping() {
//		// VR視点切り替えボタンを押して
//		// VRModeSwitch = true : 2眼
//		// VRModeSwitch = false : 単眼
//		if (PublicStaticInitializeManager.VRModeSwitch) {
//			gameManager.VRMode.VRModeEnabled = true;
//			gameManager.modeChangingVRText.text = "VR:OFF";
//		} else {
//			gameManager.VRMode.VRModeEnabled = false;
//			gameManager.modeChangingVRText.text = "VR:ON";
//		}
//	}
//}
