﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ResultManager : MonoBehaviour {
	public Canvas resultPanel;
	public Animator unityChanAnim;
	public Text progressText;
	public Cardboard VRMode;
	public Text modeChangingVRText;

	void Start () {
		// ゲームの結果に応じたアニメーションを再生してリザルトメニューを表示
		StartCoroutine ("ProgressResult");
		// VRの切り替え状態を保持する
		VRModeKeeping ();
	}

	void VRModeKeeping() {
		// VR視点切り替えボタンを押して
		// VRModeSwitch = true : 2眼
		// VRModeSwitch = false : 単眼
		if (TitleManager.VRModeSwitch) {
			VRMode.VRModeEnabled = true;
			modeChangingVRText.text = "VR:OFF";
		} else {
			VRMode.VRModeEnabled = false;
			modeChangingVRText.text = "VR:ON";
		}
	}

	// 勝利or敗北ポーズ
	void WinOrLose() {
		if (TitleManager.gameEndFlag) {	// プレイヤーが勝った
			progressText.text = "あなたのかち!";
			unityChanAnim.SetBool ("LoseTrigger", true);
		} else {	// プレイヤーが負けた
			progressText.text = "あなたのまけ!";
			unityChanAnim.SetBool ("WinTrigger", true);
		}
	}

	// ゲーム結果に対して対戦相手がリアクションしたのち
	// RETRY、TOTITLE、VR:ON/OFFのボタンが現れる
	private IEnumerator ProgressResult() {
		resultPanel.enabled = false;
		progressText.text = "";
		yield return new WaitForSeconds (0.5f);
		resultPanel.enabled = false;
		WinOrLose ();
		yield return new WaitForSeconds (2.0f);
		resultPanel.enabled = true;
	}
}
