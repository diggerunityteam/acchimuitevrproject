//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections;
//using UnityEngine.SceneManagement;
//
//public class DirectionButtonForPlayerTurn : GazedButtonClass {
//	public Text progressText;
//	public Animator unityChanAnim;
//
//	public void DecisionButton(string objDirectionButtonName) {
//		PointDirectionButton (objDirectionButtonName);
//	}
//
//	public string DecideEnemyDirection(int enemyDirectionNum, string directionButtonName) {
//		GameObject[] directionButtons;
//		directionButtons = GameObject.FindGameObjectsWithTag ("GameButton");
//		progressText.text = "ホイ!";
//		foreach (GameObject directionObject in directionButtons) {
//			if (directionObject.name == directionButtonName) {
//				directionObject.SetActive (true);
//				directionObject.GetComponent<Button> ().interactable = false;
//			} else {
//				directionObject.SetActive (false);
//			}
//		}
//		return directionButtons[enemyDirectionNum].name;
//	}
//
//	public void PointDirectionButton(string playaerDirectionButtonName) {
//		int randomNum = (int)Random.Range (0f, 4f);
//		// ジャンケンの結果に応じたシーンに遷移するコルーチンを動かす
//		StartCoroutine (GameResult(JudgeGame (DecideEnemyDirection (randomNum, playaerDirectionButtonName), playaerDirectionButtonName)));
//	}
//
//	public virtual int JudgeGame(string enemyDirection, string playerDirection) {
//		Debug.Log ("Enemy : " + enemyDirection);
//		switch (enemyDirection) {
//		case "UpButton":
//			unityChanAnim.SetBool ("UpFaceTrigger", true);
//			unityChanAnim.SetBool ("UpFaceTrigger_Reverse", true);
//			break;
//		case "RightButton":
//			unityChanAnim.SetBool ("TurnLeftTrigger", true);
//			unityChanAnim.SetBool ("TurnLeftTrigger_Reverse", true);
//			break;
//		case "DownButton":
//			unityChanAnim.SetBool ("DownFaceTrigger", true);
//			unityChanAnim.SetBool ("DownFaceTrigger_Reverse", true);
//			break;
//		default:
//			unityChanAnim.SetBool ("TurnRightTrigger", true);
//			unityChanAnim.SetBool ("TurnRightTrigger_Reverse", true);
//			break;
//		}
//		// プレイヤーが指した方向と相手の向いた方向が一致したら
//		// プレイヤーの勝利という結果をもってリザルト画面のシーン番号を返す
//		if (playerDirection == enemyDirection) {
//			PublicStaticInitializeManager.gameEndFlag = true;
//			return 4;
//		} else {	// 方向が一致しなければジャンケン画面のシーン番号を返す
//			PublicStaticInitializeManager.gameEndFlag = false;
//			return 1;
//		}
//	}
//
//	// ゲームの結果を受け取り、シーン遷移するコルーチン
//	public virtual IEnumerator GameResult(int resultNum) {
//		yield return new WaitForSeconds (1.0f);
//		unityChanAnim.SetBool ("IdleTrigger", true);
//		yield return new WaitForSeconds (1.0f);
//		if (PublicStaticInitializeManager.gameEndFlag) {
//			unityChanAnim.GetComponent<FaceAnim>().ChangeFaceAnimation (3);
//		} else {
//			unityChanAnim.GetComponent<FaceAnim>().ChangeFaceAnimation (4);
//			progressText.text = "もう1かい!";
//		}
//		yield return new WaitForSeconds (2.0f);
//		SceneManager.LoadScene (resultNum);	// 結果に応じたシーンに遷移する
//	}
//}
