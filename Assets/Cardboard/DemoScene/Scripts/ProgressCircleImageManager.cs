﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProgressCircleImageManager : MonoBehaviour {
	// レティクルを各ボタンに当ててからカウントされる経過時間
	// ボタンからレティクルが離れるとカウントが0に戻る
	private float progressTime = 0f;
	public float progressLimitTime;		// Targettingが完了するまでの時間でインスペクター上で変更できる

	void Start() {
		GetComponent<Image>().fillAmount = 0;	// Targetting画像の表示割合がリセットされる
	}

	void Update() {
		Targetting ();
	}
	// ボタンにレティクルを当てることでTargettingの画像やテキストが表示される
	// レティクルを当て続けることでTargettingが進み
	// そのまま一定時間が経過するとみていたボタンに応じた処理がかかる
	void Targetting() {
		// ボタンにレティクルを当て続けて一定時間が経過した
		if (progressTime >= progressLimitTime) {
//				progressCircleImage.enabled = false;
			// circle画像を完了する
			GetComponent<Image>().fillAmount = 1;
			// timeの値が増えすぎないようにするための予防
			progressTime = progressLimitTime;
			// ボタンを見続けて一定時間が経過すると
			// ボタンに応じた処理がかかる
//				objectNameOfUseScript = GetObjectName(true);
		} else {
			// Targettingの画像やテキストが表示されて一定時間が経過するか
			// 途中でボタンからレティクルを離すまでTargetting画像の表示割合を増やす
			CircleAppear ();
		}
	}

	public void CircleAppear() {
		// レティクルをボタンに当てている間の時間を経過させる
		progressTime += Time.deltaTime;
		// 時間経過に伴い円形に表示する画像の表示割合で最大値を1とする
		GetComponent<Image>().fillAmount = progressTime / progressLimitTime;
	}
}
