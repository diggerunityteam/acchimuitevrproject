﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GazedButtonClassForAcchi : GazedButtonClass {
	Animator unityChanAnim;

	// GameButtonタグのボタンを決定したときに呼び出される関数であっち向いてホイ用にオーバーライド
	public override void DecideGameButton(string playaerDirectionButtonName) {
		gameManager.GetComponent<AcchiManager>().progressText.text = "ホイ!";
		int randomNum = (int)Random.Range (0f, 4f);
		JudgeGame (DecideWay (randomNum, playaerDirectionButtonName),
									playaerDirectionButtonName);
		// ジャンケンの結果に応じたシーンに遷移するコルーチンを動かす
		StartCoroutine (AcchiResult (TitleManager.gameEndFlag));
	}

	// 対戦相手の手に応じたアニメーションをして、勝利判定する
	public void JudgeGame(string enemyDirection, string playerDirection) {
		unityChanAnim = gameManager.GetComponent<AcchiManager>().unityChan.GetComponent<Animator>();
		switch (enemyDirection) {
		case "UpButton":
			unityChanAnim.SetBool ("UpTrigger", true);
			break;
		case "RightButton":
			unityChanAnim.SetBool ("LeftTrigger", true);
			break;
		case "DownButton":
			unityChanAnim.SetBool ("DownTrigger", true);
			break;
		default:
			unityChanAnim.SetBool ("RightTrigger", true);
			break;
		}

		if (playerDirection == enemyDirection) {
			TitleManager.gameEndFlag = true;
			sceneNum = 4;
		} else {
			TitleManager.gameEndFlag = false;
			sceneNum = 1;
		}

		// PlayerTurnのシーンの時、正面を向きなおすアニメーションを再生
		// EnemyTurnの時、シーンに合わせるために勝利フラグを反転させる
		if ((int)SceneManager.GetActiveScene().buildIndex == 2) {
			unityChanAnim.SetBool ("ReverseTrigger", true);
		} else {
			TitleManager.gameEndFlag = !TitleManager.gameEndFlag;
		}
	}

	// ゲームの結果を受け取り、シーン遷移するコルーチン
	public IEnumerator AcchiResult(bool resultFlag) {
		yield return new WaitForSeconds (1.0f);
		gameManager.GetComponent<AcchiManager>().progressText.text = "";
		unityChanAnim.SetBool ("IdleTrigger", true);
		yield return new WaitForSeconds (1.0f);
		// PlayerTurnの時に表情アニメーションを再生する
		if ((int)SceneManager.GetActiveScene ().buildIndex == 2) {
			if (resultFlag) {
				gameManager.GetComponent<AcchiManager>().FaceAnim (3);
			} else {
				gameManager.GetComponent<AcchiManager>().FaceAnim(4);
			}
		}
		yield return new WaitForSeconds (0.5f);
		SceneManager.LoadScene (sceneNum);	// 結果に応じたシーンに遷移する
	}
}
