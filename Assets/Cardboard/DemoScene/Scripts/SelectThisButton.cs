﻿using UnityEngine;
using System.Collections;

public class SelectThisButton : MonoBehaviour {
	public bool gazingOnThisButton;

	public void SetThisButton(bool selectionThisButton) {
		gazingOnThisButton = selectionThisButton;
	}

	public bool GetThisButton() {
		return gazingOnThisButton;
	}

	public void GazeThisButton() {
		SetThisButton(true);
	}

	public void ExitThisButton() {
		SetThisButton (false);
	}
}
