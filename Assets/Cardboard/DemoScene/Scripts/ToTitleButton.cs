﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ToTitleButton : MonoBehaviour {
	private GazingButton decisionGaze;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<GazingButton> ();
	}

	// Update is called once per frame
	void Update () {
//		OnToTitleButton (this.gameObject.name.Equals(decisionGaze.buttonSwitchFlag));
	}

	void OnToTitleButton(bool decisionToTitleFlag) {
		if (decisionToTitleFlag) {
			SceneManager.LoadScene (0);	// タイトル画面に遷移
		}
	}
}
