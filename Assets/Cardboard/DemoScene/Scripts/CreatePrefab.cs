﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreatePrefab : MonoBehaviour {
	public GameObject progressCircleImagePrefab;

	void Start() {
	}

	public void ProgressPrefabInstantiate(bool selectButton) {
		if (selectButton) {
			progressCircleImagePrefab.SetActive(true);
		} else {
			progressCircleImagePrefab.SetActive(false);
		}
	}
}
